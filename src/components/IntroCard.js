import React from "react";

const CardWrapper = ({ children }) => {
  return (
    <div className="main-content">
      <div className="page-content">
        <section className="bg-auth">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-xl-10 col-lg-12">{children}</div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default CardWrapper;
