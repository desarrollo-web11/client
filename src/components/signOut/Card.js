import React from "react";
import light from "../../images/logo-light.png";
import dark from "../../images/logo-dark.png";
import singInImage from "../../images/auth/sign-in.png";
import { Link } from "react-router-dom";

const Card = () => {
  return (
    <div className="card auth-box">
      <div className="row">
        <div className="col-lg-6 text-center">
          <div className="card-body p-4">
            <Link to="/">
              <img src={light} alt="" className="logo-light" />
              <img src={dark} alt="" className="logo-dark" />
            </Link>
            <div className="mt-5">
              <img src={singInImage} alt="" className="img-fluid" />
            </div>
          </div>
        </div>
        <div className="col-lg-6">
          <div className="auth-content card-body p-5 text-white">
            <div className="w-100">
              <div className="text-center mb-4">
                <h5>You are Logged Out</h5>
                <p className="text-white-70">Thank you for using Jobcy</p>
              </div>
              <Link to="/sign-in" className="btn btn-light btn-hover w-100 ">
                Sign In
              </Link>
              <div className="mt-3 text-center">
                <p className="mb-0">
                  Don't have an account ?{" "}
                  <Link
                    to="/sign-up"
                    className="fw-medium text-white text-decoration-underline"
                  >
                    {" "}
                    Sign Up{" "}
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
