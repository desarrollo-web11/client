import React from "react";
import useLanguage from "../hooks/useLanguage";

export const Language = () => {
  const { options, translate } = useLanguage();

  return (
    <>
      <h1>{translate("hello")}</h1>
      <button onClick={() => options.changeLanguage("es")}>es</button>
      <button onClick={() => options.changeLanguage("en")}>en</button>
      <p>
        {translate("current")}: {options.language}
      </p>
    </>
  );
};
