import React from "react";
import jobImg from "../../images/featured-job/img-01.png";
import { Link } from "react-router-dom";

const Company = (props) => {
  return (
    <>
      <div className="col-lg-4">
        <div className="card side-bar">
          <div className="card-body p-4">
            <div className="candidate-profile text-center">
              <img
                src={jobImg}
                alt="company"
                className="avatar-lg rounded-circle"
              />
              <h6 className="fs-18 mb-1 mt-4">{props.nombre}</h6>
              <p className="text-muted mb-4">Since {props.fundacion}</p>
              <ul className="candidate-detail-social-menu list-inline mb-0">
                <li className="list-inline-item">
                  <a href="" className="social-link">
                    <i className="uil uil-twitter-alt"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="" className="social-link">
                    <i className="uil uil-whatsapp"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="" className="social-link">
                    <i className="uil uil-phone-alt"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="candidate-profile-overview  card-body border-top p-4">
            <h6 className="fs-17 fw-medium mb-3">Profile Overview</h6>
            <ul className="list-unstyled mb-0">
              <li>
                <div className="d-flex">
                  <label className="text-dark">Owner Name</label>
                  <div>
                    <p className="text-muted mb-0">Charles Dickens</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <label className="text-dark">Employees</label>
                  <div>
                    <p className="text-muted mb-0">1500 - 1850</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <label className="text-dark">Location</label>
                  <div>
                    <p className="text-muted mb-0">{props.ubicacion}</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <label className="text-dark">Area</label>
                  <div>
                    <p className="text-muted mb-0">{props.sector}</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <label className="text-dark">Website</label>
                  <div>
                    <p className="text-muted text-break mb-0">
                      www.Jobcytecnologypvt.ltd.com
                    </p>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <label className="text-dark">Established</label>
                  <div>
                    <p className="text-muted mb-0">July 10 2017</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <label className="text-dark">Contact</label>
                  <div>
                    <p className="text-muted mb-0">{props.telefono}</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="col-lg-8">
        <div className="card ms-lg-4 mt-4 mt-lg-0">
          <div className="card-body p-4">
            <div className="mb-5">
              <h6 className="fs-17 fw-medium mb-4">About Company</h6>
              <p className="text-muted">
                {" "}
                Objectively pursue diverse catalysts for change for
                interoperable meta-services. Distinctively re-engineer
                revolutionary meta-services and premium architectures.
                Intrinsically incubate intuitive opportunities and real-time
                potentialities. Appropriately communicate one-to-one technology.
              </p>

              <p className="text-muted">
                Intrinsically incubate intuitive opportunities and real-time
                potentialities Appropriately communicate one-to-one technology.
              </p>

              <p className="text-muted">
                {" "}
                Exercitation photo booth stumptown tote bag Banksy, elit small
                batch freegan sed. Craft beer elit seitan exercitation, photo
                booth et 8-bit kale chips proident chillwave deep v laborum.
                Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami
                readymade swag.
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-8">
          <>
            <Link to={`/companies/update/${id}`} className="btn btn-warning">
              Edit
            </Link>
            <Link to={`/companies/delete/${id}`} className="btn btn-error">
              Delete
            </Link>
          </>
        </div>
      </div>
    </>
  );
};

export default Company;
