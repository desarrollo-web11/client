import React from "react";
import light from "../../images/logo-light.png";
import dark from "../../images/logo-dark.png";
import singUpImage from "../../images/auth/sign-up.png";
import Register from "./Register";
import { Link } from "react-router-dom";

const Card = () => {
  return (
    <div className="card auth-box">
      <div className="row align-items-center">
        <div className="col-lg-6 text-center">
          <div className="card-body p-4">
            <Link to="/">
              <img src={light} alt="logo-light" className="logo-light" />
              <img src={dark} alt="logo-dark" className="logo-dark" />
            </Link>
            <div className="mt-5">
              <img src={singUpImage} alt="sign image" className="img-fluid" />
            </div>
          </div>
        </div>
        <div className="col-lg-6">
          <Register />
        </div>
      </div>
    </div>
  );
};

export default Card;
