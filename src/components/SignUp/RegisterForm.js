import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { registerUser } from "../../services/registerUser";

const RegisterForm = () => {
  const dispatch = useDispatch();

  return (
    <Formik
      initialValues={{
        username: "",
        email: "",
        password: "",
      }}
      validationSchema={Yup.object({
        username: Yup.string()
          .required("Username is required!")
          .min(6, "Username must contain at least 6 characters"),
        email: Yup.string()
          .email("Email must be valid")
          .required("Email is required!"),
        password: Yup.string()
          .required("Password is required!")
          .min(8, "Username must contain at least 8 characters"),
      })}
      onSubmit={async ({ username, password, email }, { resetForm }) => {
        try {
          const res = await registerUser({ username, password, email });
          const data = await res.json();

          console.log(data);
          resetForm();
        } catch (err) {
          console.log(err);
        }
      }}
    >
      {({}) => (
        <Form className="auth-form" noValidate>
          <div className="mb-3">
            <label htmlFor="usernameInput" className="form-label">
              Username
            </label>
            <Field
              className="form-control"
              id="usernameInput"
              placeholder="Enter your username"
              name="username"
            />
            <ErrorMessage
              name="username"
              component={"span"}
              className="form-input-error"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="passwordInput" className="form-label">
              Email
            </label>
            <Field
              className="form-control"
              id="emailInput"
              placeholder="Enter your email"
              name="email"
            />
            <ErrorMessage
              name="email"
              component={"span"}
              className="form-input-error"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="emailInput" className="form-label">
              Password
            </label>
            <Field
              type="password"
              className="form-control"
              id="passwordInput"
              placeholder="Enter your password"
              name="password"
            />

            <ErrorMessage
              name="password"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="text-center">
            <button
              type="submit"
              className="btn btn-white text-white btn-hover w-100"
            >
              Sign Up
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default RegisterForm;
