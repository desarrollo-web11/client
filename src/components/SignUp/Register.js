import React from "react";
import { Link } from "react-router-dom";
import RegisterForm from "./RegisterForm";

const Register = () => {
  return (
    <div className="auth-content card-body p-5 text-white">
      <div className="w-100">
        <div className="text-center">
          <h5>Let's Get Started</h5>
          <p className="text-white-70">
            Sign Up and get access to all the features of Jobcy
          </p>
        </div>
        <RegisterForm />
        <div className="mt-3 text-center">
          <p className="mb-0">
            Already a member ?{" "}
            <Link
              to="/sign-in"
              className="fw-medium text-white text-decoration-underline"
            >
              {" "}
              Sign In{" "}
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Register;
