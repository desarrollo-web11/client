import React from "react";
import job from "../images/featured-job/img-01.png";

export const JobBox = () => {
  return (
    <div className="job-box card mt-4">
      <div className="bookmark-label text-center">
        <a href="/" className="text-white align-middle">
          <i className="mdi mdi-star"></i>
        </a>
      </div>
      <div className="p-4">
        <div className="row align-items-center">
          <div className="col-md-2">
            <div className="text-center mb-4 mb-md-0">
              <a href="/">
                <img src={job} alt="" className="img-fluid rounded-3" />
              </a>
            </div>
          </div>

          <div className="col-md-3">
            <div className="mb-2 mb-md-0">
              <h5 className="fs-18 mb-1">
                <a href="/" className="text-dark">
                  Web Developer
                </a>
              </h5>
              <p className="text-muted fs-14 mb-0">Web Technology pvt.Ltd</p>
            </div>
          </div>

          <div className="col-md-3">
            <div className="d-flex mb-2">
              <div className="flex-shrink-0">
                <i className="mdi mdi-map-marker text-primary me-1"></i>
              </div>
              <p className="text-muted mb-0">Oakridge Lane Richardson</p>
            </div>
          </div>

          <div className="col-md-2">
            <div>
              <p className="text-muted mb-2">
                <span className="text-primary">$</span>1000-1200/m
              </p>
            </div>
          </div>

          <div className="col-md-2">
            <div>
              <span className="badge bg-soft-purple fs-13 mt-1">
                Freelancer
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="p-3 bg-light">
        <div className="row">
          <div className="col-md-4">
            <div>
              <p className="text-muted mb-0">
                <span className="text-dark">Experience :</span> 1 - 2 years
              </p>
            </div>
          </div>

          <div className="col-lg-6 col-md-5">
            <div>
              <p className="text-muted mb-0">
                <span className="text-dark">Notes :</span>
                languages only differ in their grammar.{" "}
              </p>
            </div>
          </div>

          <div className="col-lg-2 col-md-3">
            <div className="text-start text-md-end">
              <a
                href="#applyNow"
                data-bs-toggle="modal"
                className="primary-link"
              >
                Apply Now <i className="mdi mdi-chevron-double-right"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
