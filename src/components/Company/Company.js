import React from "react";
import { Link } from "react-router-dom";
import company from "../../images/featured-job/img-01.png";

const Company = ({
  img = company,
  jobs = "52",
  nombre = "Jobcy Consulting",
  ubicacion = "New York",
  puntuation = "4.9",
  sector,
  id,
  id_user,
  authId,
}) => {
  return (
    <div className="col-lg-4 col-md-6">
      <div className="card text-center mb-4">
        <div className="card-body px-4 py-5">
          <div className="featured-label">
            <span className="featured">
              {puntuation}
              <span role={"img"}>⭐</span>
            </span>
          </div>
          <img src={img} alt="" className="img-fluid rounded-3" />
          <div className="mt-4">
            <Link to={`/companies/detail/${id}`} className="primary-link">
              <h6 className="fs-18 mb-2">{nombre}</h6>
            </Link>
            <p className="text-muted mb-2">{ubicacion}</p>
            <p className="text-muted mb-4">{sector}</p>

            <Link to={`/companies/detail/${id}`} className="btn btn-primary">
              {jobs} Trabajos abiertos en esta compañia
            </Link>
            {id_user === authId ? (
              <>
                <Link
                  to={`/companies/update/${id}`}
                  className="btn btn-warning"
                >
                  Edit
                </Link>
                <Link to={`/companies/delete/${id}`} className="btn btn-error">
                  Delete
                </Link>
              </>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Company;
