import React from "react";

import { createACompany } from "../../services/companyServices";
import CompanyForm from "./CompanyForm";

const initialValues = {
  nombre: "",
  sector: "",
  ubicacion: "",
  fundacion: "",
  rif: "",
  logo: "",
  descripcion: "",
  telefono: "",
};

const CreateCompany = () => {
  return (
    <div className="row justify-content-center">
      <div
        className="d-flex flex-column card py-4 px-3 bg-light "
        style={{ maxWidth: "600px" }}
      >
        <h3 className="text-center my-3">Adding new company</h3>

        <CompanyForm sendData={createACompany} initialValues={initialValues} />
      </div>
    </div>
  );
};

export default CreateCompany;
