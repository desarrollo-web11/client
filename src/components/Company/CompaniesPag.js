import React from "react";

const CompaniesPag = () => {
  return (
    <div class="row">
      <div class="col-lg-12 mt-5">
        <nav aria-label="Page navigation example">
          <ul class="pagination job-pagination mb-0 justify-content-center">
            <li class="page-item disabled">
              <a class="page-link" href="#" tabindex="-1">
                <i class="mdi mdi-chevron-double-left fs-15"></i>
              </a>
            </li>
            <li class="page-item active">
              <a class="page-link" href="#">
                1
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                2
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                3
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                4
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                <i class="mdi mdi-chevron-double-right fs-15"></i>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default CompaniesPag;
