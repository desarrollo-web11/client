import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getCompanies } from "../../services/companyServices";
import Loader from "../Loader";
import Company from "./Company";

const CompaniesList = () => {
  const [companies, setCompanies] = useState(null);
  const {
    user: { id },
  } = useSelector((state) => state.auth);

  useEffect(() => {
    getCompanies()
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCompanies(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <section className="section" style={{ minHeight: "40vh" }}>
      <div className="container">
        <div className="row">
          {!companies ? (
            <Loader />
          ) : (
            companies?.map((company) => (
              <Company {...company} key={company.id} authId={id} />
            ))
          )}
        </div>
      </div>
    </section>
  );
};

export default CompaniesList;
