import React from "react";
import { Formik, Field, ErrorMessage, Form } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

const SCHEMA_VALIDATOR = Yup.object({
  nombre: Yup.string().required("Nombre is required"),
  sector: Yup.string().required("Sector is required"),
  ubicacion: Yup.string().required("Ubicacion is required"),
  fundacion: Yup.string().required("Fundacion is required"),
  rif: Yup.string().required("Rif is required"),
  logo: Yup.mixed().required("Logo is required"),
  descripcion: Yup.string().required("Descripcion is required"),
  telefono: Yup.string()
    .required("Numero Telefonico is required")
    .max(11, "Formato invalido"),
});

const CompanyForm = ({ initialValues, sendData }) => {
  const {
    user: { id },
  } = useSelector((state) => state.auth);
  const navigate = useNavigate();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={SCHEMA_VALIDATOR}
      onSubmit={async (data, { resetForm }) => {
        try {
          const res = await sendData({ ...data, id_user: id });

          if (!res.ok) throw { error: res.error, status: res.status };

          navigate("/companies", { replace: true });
        } catch (err) {
          console.log(err);
        }
        resetForm();
      }}
    >
      {({}) => (
        <Form>
          <div className="mb-3">
            <Field
              className="form-control"
              name="nombre"
              placeholder="Nombre..."
            />

            <ErrorMessage
              name="nombre"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="mb-3">
            <Field
              className="form-control"
              name="sector"
              placeholder="Sector..."
            />
            <ErrorMessage
              name="sector"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="mb-3">
            <Field
              className="form-control"
              name="ubicacion"
              placeholder="Ubicacion..."
            />
            <ErrorMessage
              name="ubicacion"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="mb-3">
            <Field
              className="form-control"
              name="telefono"
              placeholder="Numero telefonico..."
            />
            <ErrorMessage
              name="telefono"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="mb-3">
            <Field
              className="form-control"
              name="fundacion"
              placeholder="Fundacion..."
            />
            <ErrorMessage
              name="fundacion"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="mb-3">
            <Field className="form-control" name="rif" placeholder="Rif..." />
            <ErrorMessage
              name="rif"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="mb-3">
            <Field
              className="form-control"
              name="logo"
              placeholder="Sube tu logo"
            />
            <ErrorMessage
              name="logo"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="mb-3">
            <Field
              className="form-control"
              name="descripcion"
              placeholder="Descripcion..."
            />
            <ErrorMessage
              name="descripcion"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <button type={"submit"} className="mt-4  btn btn-primary">
            Create Company
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default CompanyForm;
