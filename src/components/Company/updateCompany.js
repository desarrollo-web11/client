import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getOneCompany, updateACompany } from "../../services/companyServices";
import CompanyForm from "./CompanyForm";

const UpdateCompany = () => {
  const { id } = useParams();
  const [companyData, setCompanyData] = useState(null);

  useEffect(() => {
    getOneCompany(parseInt(id))
      .then((data) => setCompanyData(data))
      .catch((error) => console.log(error));
  }, [id]);

  return (
    <div className="row justify-content-center">
      <div
        className="d-flex flex-column   card py-4 px-3 bg-light "
        style={{ maxWidth: "600px" }}
      >
        <h3 className="text-center my-3">Updating a company data</h3>
        {companyData && (
          <CompanyForm
            initialValues={companyData[0]}
            sendData={updateACompany}
          />
        )}
      </div>
    </div>
  );
};

export default UpdateCompany;
