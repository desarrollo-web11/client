import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { deleteACompany } from "../../services/companyServices";

const DeleteCompany = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const onDelete = () => {
    deleteACompany(parseInt(id))
      .then((data) => {
        navigate("/", { replace: true });
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="row justify-content-center my-4">
      <div className=" card py-4 px-3 bg-light " style={{ maxWidth: "600px" }}>
        <h4 className="text-center my-3 mx-auto ">Are you sure to delete?</h4>
        <button className="my-3 btn btn-success" onClick={onDelete}>
          Yes
        </button>
        <button
          className="my-3 btn btn-warning"
          onClick={() => navigate("/", { replace: true })}
        >
          No
        </button>
      </div>
    </div>
  );
};

export default DeleteCompany;
