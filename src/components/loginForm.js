import React, { useState } from "react";
import { useLoginForm } from "../hooks/useForm";
import { useDispatch } from "react-redux";
import { fetchAuthLogin } from "../feature/auth/auth";

import styled from "styled-components";

const Form = styled.form(() => ({
  display: "flex",
  flexDirection: "column",
  width: "80%",
  maxWidth: "600px",
  backgroundColor: "#eee",
  borderRadius: "8px",
  gap: "8px",
  padding: "1rem",
  margin: "auto",
}));

const ShowComponent = styled.div(() => ({
  marginLeft: "0.25rem",
  display: "flex",
  flexDirection: "row",
  gap: "8px",
  fontSize: "1.05rem",
}));

const Input = styled.input(() => ({
  borderRadius: "4px",
  border: "1px solid",
  padding: "0.30rem 1rem",
}));

const Title = styled.h3(() => ({
  display: "block",
  width: "100%",
  textAlign: "center",
}));

const SubmitButton = styled.button(() => ({
  padding: "8px",
  borderRadius: "8px",
  border: "1px solid",
}));

export const LoginForm = ({ title }) => {
  const { password, username, setUsername, setPassword } = useLoginForm();
  const [show, setShow] = useState("password");
  const dispacth = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    setPassword("");
    setUsername("");
    dispacth(fetchAuthLogin({ name: username, password }));
  };

  const handleToggleShowPassword = () => {
    show === "password" ? setShow("text") : setShow("password");
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Title>{title}</Title>
      <label>Username</label>
      <Input
        type="text"
        name="username"
        value={username}
        onChange={({ target }) => setUsername(target.value)}
      />
      <label>Password</label>
      <Input
        type={show}
        name="password"
        value={password}
        onChange={({ target }) => setPassword(target.value)}
      />
      <ShowComponent>
        <label>Show password</label>
        <Input type={"checkbox"} onClick={handleToggleShowPassword} />
      </ShowComponent>

      <SubmitButton>Log in</SubmitButton>
    </Form>
  );
};
