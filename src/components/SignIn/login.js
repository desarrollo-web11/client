import React from "react";
import { Link } from "react-router-dom";
import LoginForm from "./loginForm";

const Login = () => {
  return (
    <div className="w-100">
      <div className="text-center mb-4">
        <h5>Welcome Back !</h5>
        <p className="text-white-70">Sign in to continue to Jobcy.</p>
      </div>
      <LoginForm />
      <div className="mt-4 text-center">
        <p className="mb-0">
          Don't have an account ?{" "}
          <Link
            to="/sign-up"
            className="fw-medium text-white text-decoration-underline"
          >
            {" "}
            Sign Up{" "}
          </Link>
        </p>
      </div>
    </div>
  );
};

export default Login;
