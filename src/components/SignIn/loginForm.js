import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { fetchAuthLogin } from "../../feature/auth/auth";
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
  const navigate = useNavigate();
  const dispacth = useDispatch();
  return (
    <Formik
      initialValues={{
        username: "",
        password: "",
      }}
      onSubmit={({ username, password }, { resetForm }) => {
        dispacth(fetchAuthLogin({ name: username, password })).then(
          (result) => {
            if (result?.payload?.token) {
              navigate("/", { replace: true });
            }
          }
        );
        resetForm();
      }}
      validationSchema={yup.object({
        username: yup.string().required("The username is required"),
        password: yup.string().required("The password is required"),
      })}
    >
      {() => (
        <Form className="auth-form" noValidate>
          <div className="mb-3">
            <label htmlFor="usernameInput" className="form-label">
              Username
            </label>
            <Field
              type="text"
              className="form-control"
              id="usernameInput"
              placeholder="Enter your username"
              name="username"
            />
            <ErrorMessage
              name="username"
              component={"span"}
              className="form-input-error"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="passwordInput" className="form-label">
              Password
            </label>
            <Field
              type="password"
              className="form-control"
              id="passwordInput"
              placeholder="Enter your password"
              name="password"
            />
            <ErrorMessage
              name="password"
              component={"span"}
              className="form-input-error"
            />
          </div>

          <div className="text-center">
            <button type="submit" className="btn btn-light btn-hover w-100">
              Sign In
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default LoginForm;
