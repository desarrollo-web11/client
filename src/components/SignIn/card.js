import React from "react";
import light from "../../images/logo-light.png";
import dark from "../../images/logo-dark.png";
import singInImage from "../../images/auth/sign-in.png";
import { Link } from "react-router-dom";
import Login from "./login";

const Card = () => {
  return (
    <div className="card auth-box">
      <div className="row g-0">
        <div className="col-lg-6 text-center">
          <div className="card-body p-4">
            <Link to="/">
              <img src={light} alt="" className="logo-light" />
              <img src={dark} alt="" className="logo-dark" />
            </Link>
            <div className="mt-5">
              <img src={singInImage} alt="" className="img-fluid" />
            </div>
          </div>
        </div>
        <div className="col-lg-6">
          <div className="auth-content card-body p-5 h-100 text-white">
            <Login />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
