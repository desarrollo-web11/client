import React, { useState } from "react";

export const SignUpForm = () => {
  const [data, setData] = useState({
    username: "",
    email: "",
    password: "",
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(data);
    setData({
      username: "",
      email: "",
      password: "",
    });
  };

  return (
    <form onSubmit={handleSubmit} className="auth-form">
      <div className="mb-3">
        <label htmlFor="usernameInput" className="form-label">
          Username
        </label>
        <input
          type="text"
          className="form-control"
          required
          id="usernameInput"
          placeholder="Enter your username"
          value={data.username}
          onChange={({ target }) =>
            setData({
              ...data,
              username: target.value,
            })
          }
        />
      </div>
      <div className="mb-3">
        <label htmlFor="passwordInput" className="form-label">
          Email
        </label>
        <input
          type="email"
          className="form-control"
          required
          id="emailInput"
          placeholder="Enter your email"
          value={data.email}
          onChange={({ target }) =>
            setData({
              ...data,
              email: target.value,
            })
          }
        />
      </div>
      <div className="mb-3">
        <label htmlFor="emailInput" className="form-label">
          Password
        </label>
        <input
          type="password"
          className="form-control"
          id="passwordInput"
          placeholder="Enter your password"
          value={data.password}
          onChange={({ target }) =>
            setData({
              ...data,
              password: target.value,
            })
          }
        />
      </div>

      <div className="text-center">
        <button type="submit" className="btn btn-white btn-hover w-100">
          Sign Up
        </button>
      </div>
    </form>
  );
};
