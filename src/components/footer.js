import React from "react";

import subscribe from "../images/subscribe.png";

export const Footer = () => {
  return (
    <>
      <section className="bg-subscribe">
        <div className="container">
          <div className="row justify-content-between align-items-center">
            <div className="col-lg-6">
              <div className="text-center text-lg-start">
                <h4 className="text-white">Get New Jobs Notification!</h4>
                <p className="text-white-50 mb-0">
                  Subscribe & get all related jobs notification.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="email-img d-none d-lg-block">
          <img src={subscribe} alt="" className="img-fluid" />
        </div>
      </section>

      <section className="bg-footer">
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              <div className="footer-item mt-4 mt-lg-0 me-lg-5">
                <h4 className="text-white mb-4">Jobcy</h4>
                <p className="text-white-50">
                  It is a long established fact that a reader will be of a page
                  reader will be of at its layout.
                </p>
                <p className="text-white mt-3">Follow Us on:</p>
                <ul className="footer-social-menu list-inline mb-0">
                  <li className="list-inline-item">
                    <a href="#">
                      <i className="uil uil-facebook-f"></i>
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a href="#">
                      <i className="uil uil-linkedin-alt"></i>
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a href="#">
                      <i className="uil uil-google"></i>
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a href="#">
                      <i className="uil uil-twitter"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-6">
              <div className="footer-item mt-4 mt-lg-0">
                <p className="fs-16 text-white mb-4">Company</p>
                <ul className="list-unstyled footer-list mb-0">
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> About Us
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Contact Us
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Services
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Blog
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Team
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Pricing
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-6">
              <div className="footer-item mt-4 mt-lg-0">
                <p className="fs-16 text-white mb-4">For Jobs</p>
                <ul className="list-unstyled footer-list mb-0">
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Browser
                      Categories
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Browser Jobs
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Job Details
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Bookmark Jobs
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-6">
              <div className="footer-item mt-4 mt-lg-0">
                <p className="text-white fs-16 mb-4">For Candidates</p>
                <ul className="list-unstyled footer-list mb-0">
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Candidate List
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Candidate Grid
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Candidate
                      Details
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-6">
              <div className="footer-item mt-4 mt-lg-0">
                <p className="fs-16 text-white mb-4">Support</p>
                <ul className="list-unstyled footer-list mb-0">
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Help Center
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> FAQ'S
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="mdi mdi-chevron-right"></i> Privacy Policy
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="footer-alt">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <p className="text-white-50 text-center mb-0">
                <script>document.write(new Date().getFullYear())</script> &copy;
                Jobcy - Job Listing Page
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
