import React, { useEffect, useRef } from "react";
import { SignUpModal } from "./SignUpModal";
import dark from "../images/logo-dark.png";
import light from "../images/logo-light.png";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../feature/auth/auth";

export const Topbar = () => {
  const navBar = useRef(null);
  const [isSticky, setIsSticky] = useState(false);
  const {
    user: { token, name },
  } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  useEffect(() => {
    const handlerScrolling = (navBar) => {
      if (!navBar) return;
      if (navBar.offsetHeight < window.scrollY) {
        setIsSticky(true);
      } else if (navBar.offsetHeight > window.scrollY) {
        setIsSticky(false);
      }
    };
    window.addEventListener("scroll", () => handlerScrolling(navBar.current));
    return () => {
      window.removeEventListener("scroll", () =>
        handlerScrolling(navBar.current)
      );
    };
  }, []);

  return (
    <>
      <nav
        className={`navbar navbar-expand-lg ${
          isSticky ? "sticky-top" : "fixed-top"
        }`}
        id="navbar"
        ref={navBar}
      >
        <div className="container-fluid custom-container">
          <Link className="navbar-brand text-dark fw-bold me-auto" to="/">
            <img src={dark} height="22" alt="logo-dark" className="logo-dark" />
            <img
              src={light}
              height="22"
              alt="logo-light"
              className="logo-light"
            />
          </Link>
          <div>
            <button
              className="navbar-toggler me-3"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarCollapse"
              aria-controls="navbarCollapse"
              aria-label="Toggle navigation"
            >
              <i className="bi bi-list" style={{ color: "black" }}></i>
            </button>
          </div>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav mx-auto navbar-center">
              <li className="nav-item dropdown dropdown-hover">
                <Link className="nav-link" to="/">
                  Inicio
                </Link>
              </li>
              <li className="nav-item dropdown dropdown-hover">
                <a
                  className="nav-link"
                  href=""
                  id="jobsdropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                >
                  Postulaciones <i className="bi bi-chevron-down"></i>
                </a>
                <ul
                  className="dropdown-menu dropdown-menu-center"
                  aria-labelledby="jobsdropdown"
                >
                  <li>
                    <a className="dropdown-item" href="">
                      About Us
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown dropdown-hover">
                <a
                  className="nav-link"
                  href=""
                  id="pagesdoropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                >
                  Vacantes <i className="bi bi-chevron-down"></i>
                </a>
                <div
                  className="dropdown-menu dropdown-menu-lg dropdown-menu-center "
                  aria-labelledby="pagesdoropdown"
                  style={{ width: "max-content" }}
                >
                  <div className="row">
                    <div className="col-lg-4">
                      <span className="dropdown-header">Jobs</span>
                      <div>
                        <a className="dropdown-item" href="#">
                          Job List
                        </a>
                        <a className="dropdown-item" href="#">
                          Job List-2
                        </a>
                        <a className="dropdown-item" href="#">
                          Job Grid
                        </a>
                        <a className="dropdown-item" href="#">
                          Job Grid-2
                        </a>
                        <a className="dropdown-item" href="#">
                          Job Details
                        </a>
                        <a className="dropdown-item" href="#">
                          Jobs Categories
                        </a>
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <span className="dropdown-header">
                        Candidates / Companys
                      </span>
                      <div>
                        <a className="dropdown-item" href="#">
                          Candidate List
                        </a>
                        <a className="dropdown-item" href="#">
                          Candidate Grid
                        </a>
                        <a className="dropdown-item" href="#">
                          Candidate Details
                        </a>
                        <a className="dropdown-item" href="#">
                          Company List
                        </a>
                        <a className="dropdown-item" href="#">
                          Company Details
                        </a>
                      </div>
                    </div>
                    {/*<div className="col-lg-4">
                                        <span className="dropdown-header">Extra Pages</span>
                                        <div>
                                            <a className="dropdown-item" href="sign-up.html">Sign Up</a>
                                            <a className="dropdown-item" href="sign-in.html">Sign In</a>
                                            <a className="dropdown-item" href="sign-out.html">Sign Out</a>
                                            <a className="dropdown-item" href="reset-password.html">Reset Password</a>
                                            <a className="dropdown-item" href="coming-soon.html">Coming Soon</a>
                                            <a className="dropdown-item" href="404-error.html">404 Error</a>
                                            <a className="dropdown-item" href="components.html">Components</a>
                                        </div>
                                     </div>*/}
                  </div>
                </div>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/companies">
                  Compañias
                </Link>
              </li>
              <li className="nav-item">
                <a href="/" className="nav-link">
                  Contacto
                </a>
              </li>
            </ul>
          </div>

          <div className="header-menu list-inline d-flex justify-content-center align-items-center">
            {token ? (
              <>
                <div className="list-inline-item me-4">{name}</div>
                <div className="list-inline-item me-4">
                  <Link
                    className="nav-link btn btn-danger py-1 px-2"
                    to="/sign-out"
                    onClick={() => dispatch(logout())}
                  >
                    Logout
                  </Link>
                </div>
              </>
            ) : (
              <>
                <div className="list-inline-item me-4">
                  <Link
                    to="/sign-up"
                    className="nav-link btn btn-light py-1 px-2"
                  >
                    Registro
                  </Link>
                </div>
                <div className="list-inline-item me-4">
                  <Link
                    to="/sign-in"
                    className="nav-link btn btn-outline-secondary py-1 px-2"
                  >
                    Iniciar sesión
                  </Link>
                </div>
              </>
            )}
          </div>

          {/** USER INFORMATION 
            <ul className="header-menu list-inline d-flex align-items-center mb-0">
            <li className="list-inline-item dropdown me-4">
              <a
                href=""
                className="header-item noti-icon position-relative"
                id="notification"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <i className="bi bi-bell"></i>
                <div className="count position-absolute">3</div>
              </a>
              <div
                className="dropdown-menu dropdown-menu-sm dropdown-menu-end p-0"
                aria-labelledby="notification"
              >
                <div className="notification-header border-bottom bg-light">
                  <h6 className="mb-1"> Notification </h6>
                  <p className="text-muted fs-13 mb-0">
                    You have 4 unread Notification
                  </p>
                </div>
                <div className="notification-wrapper dropdown-scroll">
                  <a
                    href=""
                    className="text-dark notification-item d-block active"
                  >
                    <div className="d-flex align-items-center">
                      <div className="flex-shrink-0 me-3">
                        <div className="avatar-xs bg-primary text-white rounded-circle text-center">
                          <i className="uil uil-user-check"></i>
                        </div>
                      </div>
                      <div className="flex-grow-1">
                        <h6 className="mt-0 mb-1 fs-14">
                          22 verified registrations
                        </h6>
                        <p className="mb-0 fs-12 text-muted">
                          <i className="mdi mdi-clock-outline"></i>{" "}
                          <span>3 min ago</span>
                        </p>
                      </div>
                    </div>
                  </a>
                  <a
                    href="#"
                    className="text-dark notification-item d-block"
                  >
                    <div className="d-flex align-items-center">
                      <div className="flex-shrink-0 me-3">
                        <img
                          src={avatar1}
                          className="rounded-circle avatar-xs"
                          alt="user-pic"
                        />
                      </div>
                      <div className="flex-grow-1">
                        <h6 className="mt-0 mb-1 fs-14">James Lemire</h6>
                        <p className="text-muted fs-12 mb-0">
                          <i className="mdi mdi-clock-outline"></i>{" "}
                          <span>15 min ago</span>
                        </p>
                      </div>
                    </div>
                  </a>
                  <a
                    href="#"
                    className="text-dark notification-item d-block"
                  >
                    <div className="d-flex align-items-center">
                      <div className="flex-shrink-0 me-3">
                        <img
                          src={avatar2}
                          className="rounded-circle avatar-xs"
                          alt="user-pic"
                        />
                      </div>
                      <div className="flex-grow-1">
                        <h6 className="mt-0 mb-1 fs-14">
                          Applications has been approved
                        </h6>
                        <p className="text-muted mb-0 fs-12">
                          <i className="mdi mdi-clock-outline"></i>{" "}
                          <span>45 min ago</span>
                        </p>
                      </div>
                    </div>
                  </a>
                  <a
                    href="#"
                    className="text-dark notification-item d-block"
                  >
                    <div className="d-flex align-items-center">
                      <div className="flex-shrink-0 me-3">
                        <img
                          src={avatar3}
                          className="rounded-circle avatar-xs"
                          alt="user-pic"
                        />
                      </div>
                      <div className="flex-grow-1">
                        <h6 className="mt-0 mb-1 fs-14">Kevin Stewart</h6>
                        <p className="text-muted mb-0 fs-12">
                          <i className="mdi mdi-clock-outline"></i>{" "}
                          <span>1 hour ago</span>
                        </p>
                      </div>
                    </div>
                  </a>
                  <a
                    href="#"
                    className="text-dark notification-item d-block"
                  >
                    <div className="d-flex align-items-center">
                      <div className="flex-shrink-0 me-3">
                        <img
                          src={avatar4}
                          className="rounded-circle avatar-xs"
                          alt="user-pic"
                        />
                      </div>
                      <div className="flex-grow-1">
                        <h6 className="mt-0 mb-1 fs-15">Creative Agency</h6>
                        <p className="text-muted mb-0 fs-12">
                          <i className="mdi mdi-clock-outline"></i>{" "}
                          <span>2 hour ago</span>
                        </p>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="notification-footer border-top text-center">
                  <a className="primary-link fs-13" href="#">
                    <i className="mdi mdi-arrow-right-circle me-1"></i>{" "}
                    <span>View More..</span>
                  </a>
                </div>
              </div>
            </li>
            <li className="list-inline-item dropdown">
              <a
                href="#"
                className="header-item"
                id="userdropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img
                  src={profile}
                  alt="mdo"
                  width="35"
                  height="35"
                  className="rounded-circle me-1"
                />{" "}
                <span className="d-none d-md-inline-block fw-medium">
                  Hi, Jansh
                </span>
              </a>
              <ul
                className="dropdown-menu dropdown-menu-end"
                aria-labelledby="userdropdown"
              >
                <li>
                  <Link className="dropdown-item" to="manage-jobs.html">
                    Manage Jobs
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="bookmark-jobs.html">
                    Bookmarks Jobs
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="profile.html">
                    My Profile
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="signout">
                    Logout
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
            
            
           */}
        </div>
      </nav>

      <div
        className="modal fade"
        id="signupModal"
        tabIndex="-1"
        aria-hidden="true"
      >
        <SignUpModal />
      </div>
    </>
  );
};
