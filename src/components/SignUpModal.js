import React from "react";
import { Link } from "react-router-dom";
import { SignUpForm } from "../components/signUpForm";

export const SignUpModal = () => {
  return (
    <div className="modal-dialog modal-dialog-centered">
      <div className="modal-content">
        <div className="modal-body p-5">
          <div className="position-absolute end-0 top-0 p-3">
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="auth-content">
            <div className="w-100">
              <div className="text-center mb-4">
                <h5>Sign Up</h5>
                <p className="text-muted">
                  Sign Up and get access to all the features of Jobcy
                </p>
              </div>
              <SignUpForm />
              <div className="mt-3 text-center">
                <p className="mb-0">
                  Already a member ?{" "}
                  <Link
                    to="sign-in"
                    className="form-text text-primary text-decoration-underline"
                  >
                    {" "}
                    Sign in{" "}
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
