import React from "react";
import { JobBox } from "./jobBox";

export const ListJobs = () => {
  return (
    <section className="section bg-light">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="section-title text-center mb-4 pb-2">
              <h4 className="title">Vacantes</h4>
              <p className="text-muted mb-1">
                Publique un trabajo para contarnos su proyecto. Rápidamente te
                pondremos en contacto con los freelancers adecuados.
              </p>
            </div>
          </div>
        </div>

        <div className="col-lg-12">
          <div className="tab-pane" id="featured-jobs" role="tabpanel">
            <JobBox />
          </div>
        </div>
      </div>
    </section>
  );
};
