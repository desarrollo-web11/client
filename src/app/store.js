import { configureStore } from "@reduxjs/toolkit";
import auhtReducer from "../feature/auth/auth";

const store = configureStore({
  reducer: {
    auth: auhtReducer,
  },
});

export default store;
