const { API_URL } = process.env;

export const getCompanies = () => {
  return fetch(`${API_URL}/api/empresa/index `);
};

export const getOneCompany = async (id) => {
  try {
    const res = await getCompanies();
    const data = await res.json();

    const companyFound = data.filter((company) => company.id === id);

    if (!companyFound) throw { error: "Company doesn´t exist" };

    return companyFound;
  } catch (err) {
    return err;
  }
};

export const createACompany = (data) => {
  return fetch(`${API_URL}/api/empresa/store`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
};

export const updateACompany = (data) => {
  console.log(data);
  return fetch(`${API_URL}/api/empresa/update`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
};

export const deleteACompany = (id) => {
  return fetch(`${API_URL}/api/empresa/delete`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ id }),
  });
};
