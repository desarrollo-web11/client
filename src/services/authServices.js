export const loginService = ({ name, password }) => {
  return fetch(`${process.env.API_URL}/api/usuario/login`, {
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ name, password }),
  });
};

const logoutService = (data) => {};
