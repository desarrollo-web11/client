const { API_URL } = process.env;

export const registerUser = ({ username, email, password }) => {
  return fetch(`${API_URL}/api/usuario/register`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ name: username, email, password }),
  });
};
