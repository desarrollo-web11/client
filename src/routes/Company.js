import React from "react";
import { useNavigate } from "react-router-dom";
import CompaniesList from "../components/Company/CompaniesList";
import CreateCompany from "../components/Company/CreateCompany";
import Modal from "../components/modal";
import { PageTitle } from "../components/pageTitle";
import { Topbar } from "../components/topbar";

const Company = () => {
  const navigate = useNavigate();

  return (
    <>
      <Topbar />
      <PageTitle pageTitle={"Company list"} title="Companies" />
      <section className="section">
        <div className="container">
          <button
            type="button"
            className="btn btn-success"
            data-bs-toggle="modal"
            data-bs-target="#staticBackdrop"
            onClick={() => navigate("/companies/create")}
          >
            Add new Company
          </button>

          <div className="row align-items-center mb-4">
            <div className="col-lg-8">
              <div className="mb-3 mb-lg-0">
                <h6 className="fs-16 mb-0"> Showing 1 – 8 of 11 results </h6>
              </div>
            </div>

            <div className="col-lg-4">
              <div className="candidate-list-widgets">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="selection-widget">
                      <select
                        className="form-select"
                        data-trigger
                        name="choices-single-filter-orderby"
                        id="choices-single-filter-orderby"
                        aria-label="Default select example"
                      >
                        <option value="df">Default</option>
                        <option value="ne">Newest</option>
                        <option value="od">Oldest</option>
                        <option value="rd">Random</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="selection-widget mt-2 mt-lg-0">
                      <select
                        className="form-select"
                        data-trigger
                        name="choices-candidate-page"
                        id="choices-candidate-page"
                        aria-label="Default select example"
                      >
                        <option value="df">All</option>
                        <option value="ne">8 per Page</option>
                        <option value="ne">12 per Page</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <CompaniesList />
        </div>
      </section>
    </>
  );
};

export default Company;
