import React from "react";
import DeleteCompany from "../components/Company/DeleteCompany";
import { PageTitle } from "../components/pageTitle";
import { Topbar } from "../components/topbar";

const CompanyToDelete = () => {
  return (
    <>
      <Topbar />
      <PageTitle pageTitle={"Company Form"} title="Add company" />
      <DeleteCompany />
    </>
  );
};

export default CompanyToDelete;
