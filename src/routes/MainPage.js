import React from "react";
import { Topbar } from "../components/topbar";
import { Footer } from "../components/footer";
import { PageTitle } from "../components/pageTitle";
import { ListJobs } from "../components/listJobs";
import CompaniesList from "../components/Company/CompaniesList";

export const MainPage = () => {
  return (
    <div>
      <Topbar />
      <PageTitle pageTitle={"Main"} title="Página principal" />
      <ListJobs />
      <CompaniesList />
    </div>
  );
};
