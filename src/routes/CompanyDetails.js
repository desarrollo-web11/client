import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getOneCompany } from "../services/companyServices";
import Company from "../components/CompanyDetails/Company";
import Loader from "../components/Loader";
import { PageTitle } from "../components/pageTitle";
import { Topbar } from "../components/topbar";

const CompanyList = () => {
  const { id } = useParams();
  const [companyData, setCompanyData] = useState(null);

  useEffect(() => {
    getOneCompany(parseInt(id))
      .then((res) => setCompanyData(res))
      .catch((err) => console.log(err));
  }, []);

  console.log(companyData);

  return (
    <>
      <Topbar />
      <PageTitle
        title={"Company Details"}
        pageTitle={`${companyData && companyData[0].nombre} Details`}
      />
      <section className="section">
        <div className="container">
          <section className="row">
            {companyData ? <Company {...companyData[0]} /> : <Loader />}
          </section>
        </div>
      </section>
    </>
  );
};

export default CompanyList;
