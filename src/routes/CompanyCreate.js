import React from "react";
import CreateCompany from "../components/Company/CreateCompany";
import { PageTitle } from "../components/pageTitle";
import { Topbar } from "../components/topbar";

const CompanyCreate = () => {
  return (
    <>
      <Topbar />
      <PageTitle pageTitle={"Company Form"} title="Add company" />
      <section className="section">
        <div className="container">
          <CreateCompany />
        </div>
      </section>
    </>
  );
};

export default CompanyCreate;
