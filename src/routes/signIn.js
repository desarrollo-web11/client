import React from "react";
import CardWrapper from "../components/IntroCard";
import { Card } from "../components/SignIn";

export const SignIn = () => {
  return (
    <CardWrapper>
      <Card />
    </CardWrapper>
  );
};
