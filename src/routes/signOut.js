import React from "react";
import CardWrapper from "../components/IntroCard";
import Card from "../components/signOut/Card";

export const SignOut = () => {
  return (
    <CardWrapper>
      <Card />
    </CardWrapper>
  );
};
