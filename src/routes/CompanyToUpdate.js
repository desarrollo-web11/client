import React from "react";
import UpdateCompany from "../components/Company/updateCompany";
import { PageTitle } from "../components/pageTitle";
import { Topbar } from "../components/topbar";

const CompanyToUpdate = () => {
  return (
    <>
      <Topbar />

      <PageTitle pageTitle={"Company Form"} title="Update company" />
      <section className="section">
        <div className="container">
          <UpdateCompany />
        </div>
      </section>
    </>
  );
};

export default CompanyToUpdate;
