import React, { StrictMode } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { App } from "./app";
import "./i18next-config.js";
import store from "./app/store";
import { Provider } from "react-redux";

import { Footer } from "./components/footer";
import { PageTitle } from "./components/pageTitle";
import { Topbar } from "./components/topbar";
import "./styles/app.scss";

import imgF from "./images/process-03.png";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </StrictMode>
);
