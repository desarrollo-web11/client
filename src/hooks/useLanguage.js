import { useTranslation } from "react-i18next";

const useLanguage = () => {
  const { t, i18n } = useTranslation("translation");

  return {
    translate: t,
    options: i18n,
  };
};

export default useLanguage;
