import { useCallback, useState } from "react";

export const useLoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const login = useCallback((state, action) => {
    console.log(action);
    loginService(action.payload)
      .then((res) => {
        console.log(res);
        return res.json();
      })
      .then((data) => {
        console.log(data);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return {
    username,
    password,
    setPassword,
    setUsername,
    login,
  };
};
