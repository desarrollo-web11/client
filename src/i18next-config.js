import { initReactI18next } from "react-i18next";
import i18next from "i18next";
import global_es from "./translations/es/global.json";
import global_en from "./translations/en/global.json";

i18next.use(initReactI18next).init({
  interpolation: { escapeValue: false },
  lng: "en",
  fallbackLng: "en",
  resources: {
    es: {
      ...global_es,
    },
    en: {
      ...global_en,
    },
  },
});
