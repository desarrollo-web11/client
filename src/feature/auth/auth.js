import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { loginService } from "../../services/authServices";

const user = JSON.parse(localStorage.getItem("AUTH_TOKEN")) || {
  username: null,
  token: null,
};

export const fetchAuthLogin = createAsyncThunk(
  "fetch/fetchAuthLogin",
  async (credentials) => {
    try {
      const res = await loginService(credentials);
      const data = await res.json();

      return data;
    } catch (err) {
      return err.message;
    }
  }
);

export const AuthSlice = createSlice({
  name: "auth",
  initialState: {
    user,
    status: "idle",
    error: null,
  },
  reducers: {
    login: (state, action) => {},
    logout: (state, _) => {
      state.user.token = null;
      state.user.username = null;
      localStorage.removeItem("AUTH_TOKEN");
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchAuthLogin.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(fetchAuthLogin.fulfilled, (state, action) => {
        state.status = "succeded";

        const { token } = action.payload;
        if (token) {
          const {
            user: { name, email, id },
          } = action.payload;
          state.user = {
            token,
            name,
            email,
            id,
          };
        }
        localStorage.setItem("AUTH_TOKEN", JSON.stringify(state.user));
      })
      .addCase(fetchAuthLogin.rejected, (state, action) => {
        state.status = "failed";
      });
  },
});

export const { login, logout } = AuthSlice.actions;

export default AuthSlice.reducer;
