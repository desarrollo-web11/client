import React from "react";
import { Footer } from "./components/footer";

import { Route, Routes } from "react-router-dom";
import { SignIn } from "./routes/signIn";
import { SignOut } from "./routes/signOut";
import { SignUp } from "./routes/SignUp";
import { MainPage } from "./routes/MainPage";
import Company from "./routes/Company";
import CreateCompany from "./components/Company/CreateCompany";
import CompanyDetails from "./routes/CompanyDetails";
import UpdateCompany from "./components/Company/updateCompany";
import DeleteCompany from "./components/Company/DeleteCompany";
import CompanyCreate from "./routes/CompanyCreate";
import CompanyToUpdate from "./routes/CompanyToUpdate";
import CompanyToDelete from "./routes/CompanyTodelete";

export const App = () => {
  return (
    <Routes>
      <Route path="/" element={<MainPage />} />
      <Route path="sign-out" element={<SignOut />} />
      <Route path="sign-in" element={<SignIn />} />
      <Route path="sign-up" element={<SignUp />} />

      <Route path="companies" element={<Company />} />
      <Route path="companies/create" element={<CompanyCreate />} />
      <Route path="companies/detail/:id" element={<CompanyDetails />} />
      <Route path="companies/update/:id" element={<CompanyToUpdate />} />

      <Route path="companies/delete/:id" element={<CompanyToDelete />} />
      <Route path="*" element={<h2>404</h2>} />
    </Routes>

    // <>
    //   <div className="main-content">
    //     <div className="page-content">
    //       <Footer />
    //     </div>
    //   </div>
    // </>
  );
};
