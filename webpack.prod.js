const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.common");
const { DefinePlugin } = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");

const prodConfig = {
  mode: "production",
  plugins: [
    new DefinePlugin({
      "process.env": JSON.stringify({
        API_URL: process.env.API_URL_PROD,
      }),
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "./public/_redirects",
          to: "./",
        },
      ],
    }),
  ],
};

module.exports = merge(commonConfig, prodConfig);
