const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.common");
const { DefinePlugin } = require("webpack");
require("dotenv").config();
const path = require("path");

const devConfig = {
  mode: "development",
  output: {
    filename: "[name].js",
    path: path.resolve(process.cwd(), "dist"),
    clean: true,
    publicPath: "/",
  },
  devServer: {
    static: "./",
    historyApiFallback: true,
    open: true,
  },
  plugins: [
    new DefinePlugin({
      "process.env": JSON.stringify({
        API_URL: process.env.API_URL_DEV,
      }),
    }),
  ],
};

module.exports = merge(commonConfig, devConfig);
